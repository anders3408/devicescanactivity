/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.android.bluetoothlegatt.BluetoothLeService;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity  {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private Button mDeviceState;
    private ToggleButton mOneClickButton;
    private ToggleButton mTwoClickButton;
    private MenuItem mSwitchItem;
    private Switch sEnable;
    private TextView mConnectionState;
    private TextView mConnectionRssi;
    private String mDeviceName;
    private String mDeviceAddress;
    private String mDeviceRssi;
    private BluetoothLeService mBluetoothLeService;
    private BluetoothAdapter mBluetoothAdapter;
    private Intent uiIntent;

    private static boolean isReceiverRegistered = false;
    private static boolean mConnected = false;
    private static boolean mEnabled = false;
    public static boolean mOneClick = false;
    public static boolean mTwoClick = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        preSetup();
        
        uiIntent = new Intent(this, BluetoothLeService.class);
        
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        
        getActionBar().setTitle(mDeviceName);
        mDeviceState = (Button) findViewById(R.id.device_state);
        mDeviceState.setEnabled(mDeviceAddress != null);
        
        if (mDeviceAddress != null){
        	if (!isReceiverRegistered) {
        		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        		registerReceiver(broadcastReceiver, new IntentFilter(BluetoothLeService.UPDATE_UI));
        		isReceiverRegistered = true;
        	}
        	onConnected();
        } else
        	onDisconnected();
    }
    
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	updateUI(intent);       
        }
    }; 
    
    private void updateUI(Intent intent) {
    	String rssi = intent.getStringExtra("DEVICE_RSSI");
    	mConnectionRssi = (TextView) findViewById(R.id.rssi_text);
    	mConnectionRssi.setText(rssi);
    }

    @Override
    protected void onResume() {
        super.onResume();
        
        if(mDeviceAddress == null || (BluetoothLeService.mBluetoothDeviceAddress != null))
        	mDeviceAddress = BluetoothLeService.mBluetoothDeviceAddress;

        // Sets up UI references.
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        
        if (mDeviceAddress != null){
        	if (!isReceiverRegistered) {
        		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        		registerReceiver(broadcastReceiver, new IntentFilter(BluetoothLeService.UPDATE_UI));
        		updateConnectionState(R.string.connected);
        		isReceiverRegistered = true;
        	}
        	onConnected();
        } else
        	onDisconnected();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isReceiverRegistered) {
       		try {
       			unregisterReceiver(broadcastReceiver);
               	unregisterReceiver(mGattUpdateReceiver);
               	isReceiverRegistered = false;
           	} catch (IllegalArgumentException e) {
           		Log.d(TAG, "IllegalArgumentException on unregisterReceiver");
           		e.printStackTrace();
           	}
       }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isReceiverRegistered) {
       		try {
       			unregisterReceiver(broadcastReceiver);
               	unregisterReceiver(mGattUpdateReceiver);
               	isReceiverRegistered = false;
           	} catch (IllegalArgumentException e) {
           		Log.d(TAG, "IllegalArgumentException on unregisterReceiver");
           		e.printStackTrace();
           	}
       }
    }

    @Override
	public void onStart() {
		super.onStart();
		bindService(new Intent(this, BluetoothLeService.class), mServiceConnection, 0);
	}

	@Override
	public void onStop() {
		super.onStop();
		unbindService(mServiceConnection);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        mSwitchItem = menu.findItem(R.id.action_switch);
        sEnable = (Switch) mSwitchItem.getActionView().findViewById(
				R.id.switchForActionBar);
        sEnable.setChecked(mDeviceAddress != null);
        mDeviceState.setEnabled(mDeviceAddress != null);
        sEnable.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				Boolean sState = sEnable.isChecked();

				if(sState) {
					startService();
					SwitchIsOn();
				}
				else {
					stopService();
					SwitchIsOff();
				}
			}
		});

		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case R.id.action_switch:
        	mSwitchItem = item;
        	break;
        case android.R.id.home:
        	finish();
        	break;
		default:
			break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
                preSetup();
                onConnected();
                onDisconnected();
            }
        });
    }
    
    public void startService() {
    	startService(new Intent(DeviceControlActivity.this, BluetoothLeService.class));
    }

    public void stopService() {
    	stopService(new Intent(DeviceControlActivity.this, BluetoothLeService.class));
    }
    
    private void preSetup(){
    	// Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        if(mDeviceAddress == null || (BluetoothLeService.mBluetoothDeviceAddress != null))
        	mDeviceAddress = BluetoothLeService.mBluetoothDeviceAddress;

        // Sets up UI references.
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        
        mOneClickButton = (ToggleButton) findViewById(R.id.oneClickButton);
        mOneClickButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
        	@Override
            public void onCheckedChanged(CompoundButton buttonView, 
            		boolean isChecked) {
				if (isChecked) {
					Toast.makeText(getApplicationContext(),"Switch is on", Toast.LENGTH_SHORT).show();
					mOneClick = true;
				} else {
					Toast.makeText(getApplicationContext(),"Switch is off", Toast.LENGTH_SHORT).show();
					mOneClick = false;
				}
			}
		});
        
        mTwoClickButton = (ToggleButton) findViewById(R.id.twoClickButton);
        mTwoClickButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
        	@Override
            public void onCheckedChanged(CompoundButton buttonView, 
            		boolean isChecked) {
				if (isChecked) {
					Toast.makeText(getApplicationContext(),"Switch is on", Toast.LENGTH_SHORT).show();
					mTwoClick = true;
				} else {
					Toast.makeText(getApplicationContext(),"Switch is off", Toast.LENGTH_SHORT).show();
					mTwoClick = false;
				}
			}
		});
 
    }

    private void SwitchIsOff(){
        mDeviceState.setEnabled(false);
    	mConnected = false;
    	onDisconnected();

    	if (BluetoothLeService.mIsRunning) {
    		mBluetoothLeService = null;
    	}
    }

    private void SwitchIsOn(){
    	preSetup();
        mDeviceState.setEnabled(true);
    }

    private void onConnected(){
    	if (mConnected || mDeviceAddress != null) {
    		mDeviceState = (Button) findViewById(R.id.device_state);
        	mDeviceState.setText(R.string.disconnect_device);
        	mDeviceState.setOnClickListener(new OnClickListener() {
        		@Override
        		public void onClick(View v) {
        			mBluetoothLeService.disconnect();
        			mConnected = false;
        			updateConnectionState(R.string.disconnected);
        			onDisconnected();
        		}
        	});
        }
    }

    private void onDisconnected(){
    	if (!mConnected) {
    		mConnectionRssi = (TextView) findViewById(R.id.rssi_text);
    		mConnectionRssi.setText(" ");
    		mDeviceState = (Button) findViewById(R.id.device_state);
        	mDeviceState.setText(R.string.scan_device);
        	mDeviceState.setOnClickListener(new OnClickListener() {
         		@Override
         		public void onClick(View v) {
         			Intent DeviceStateIntent = new Intent (getApplicationContext(), DeviceScanActivity.class);
         			startActivity(DeviceStateIntent);
         		}
         	});
         }
    }

    private void clearUI() {
        mDeviceState.setEnabled(true);
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            if (BluetoothLeService.mIsRunning) {
            	if (!BluetoothLeService.mConnected){
            		// Automatically connects to the device upon successful start-up initialization.
            		mBluetoothLeService.connect(mDeviceAddress);
            	}
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            }
        }
    };

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

}

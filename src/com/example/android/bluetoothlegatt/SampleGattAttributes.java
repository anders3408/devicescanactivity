/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import java.util.HashMap;
import java.util.UUID;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
	private static HashMap<String, String> attributes = new HashMap();
    // Service
    public static UUID SIMPLE_KEY_SERVICE = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    public static String IMM_ALERT_SERVICE = "00001802-0000-1000-8000-00805f9b34fb";
    public static String LINK_LOSS_SERVICE = "00001803-0000-1000-8000-00805f9b34fb";
    public static String TRANSMIT_POWER_SERVICE = "00001804-0000-1000-8000-00805f9b34fb";
    public static String OAD_SERVICE = "f000ffc0-0451-4000-b000-000000000000";

    // Characterstic
    public static UUID SIMPLE_KEY_CHARv1 = UUID.fromString("f000ffe1-0451-4000-b000-000000000000");
    public static UUID SIMPLE_KEY_CHARv2 = UUID.fromString("f000ffe2-0451-4000-b000-000000000000");
    public static UUID SIMPLE_KEY_CHARv3 = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
    public static String IMM_ALERT_LEVEL = "00002a06-0000-1000-8000-00805f9b34fb";
    public static String LINK_LOSS_LEVEL = "00002a06-0000-1000-8000-00805f9b34fb";
    public static String TRANSMIT_POWER_CHAR = "00002a07-0000-1000-8000-00805f9b34fb";
    public static String OAD_CHARv1 = "f000ffc1-0451-4000-b000-000000000000";
    public static String OAD_CHARv2 = "f000ffc2-0451-4000-b000-000000000000";
    public static String DEVICE_NAME = "00002a00-0000-1000-8000-00805f9b34fb";

    static {
        // Services.
   // 	attributes.put(SIMPLE_KEY_SERVICE, "Simple Key Service");
        attributes.put(IMM_ALERT_SERVICE, "Immediate Alert Service");
        attributes.put(LINK_LOSS_SERVICE, "Link Loss Service");
        attributes.put(TRANSMIT_POWER_SERVICE, "Transmit Power Service");
        attributes.put(OAD_SERVICE, "Over The Air Service");
        // Characteristics.
   //     attributes.put(SIMPLE_KEY_CHARv1, "Simple Key Characteristic v1");
   //     attributes.put(SIMPLE_KEY_CHARv2, "Simple Key Characteristic v2");
   //     attributes.put(SIMPLE_KEY_CHARv3, "Simple Key Characteristic v3");
        attributes.put(IMM_ALERT_LEVEL, "Immediate Alert Level");
        attributes.put(LINK_LOSS_LEVEL, "Link Loss Alert Level");
        attributes.put(TRANSMIT_POWER_CHAR, "Transmit Power Char");
        attributes.put(OAD_CHARv1, "Over The Air v1");
        attributes.put(OAD_CHARv2, "Over The Air v2");
        attributes.put(DEVICE_NAME, "Device Name");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
